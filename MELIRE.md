# Plateform.io

Install
```
brew install platformio
```

Build
```
rm -f firmware.hex && platformio run -e sanguino_atmega1284p && cp .pioenvs/sanguino_atmega1284p/firmware.hex firmware.hex
```

Firmware location : `.pioenvs/sanguino_atmega1284p/firmware.hex`

# Configuration Arduino IDE

* Tools > 
  * Board : Sanguino
  * Processor : ATMega 1284P 16MHz

Source : https://howchoo.com/g/mge1mdfkzjv/ender-3-bootloader-firmware-update-marlin

# Optimisation de la taille du firmware

Créer le fichier ~/Library/Arduino15/packages/arduino/hardware/avr/1.8.2/platform.local.txt

```
compiler.c.extra_flags=-fno-tree-scev-cprop -fno-split-wide-types -Wl,--relax -mcall-prologues
compiler.c.elf.extra_flags=-Wl,--relax
compiler.cpp.extra_flags=-fno-tree-scev-cprop -fno-split-wide-types -Wl,--relax -mcall-prologues
```
Exemple : 
Taille d'origine : 125036 octets
Nouvelle taille  :  119550
*Gain*             : 5486

# Octoprint

GCode start : 
```
M117 Calibrating
M201 X500.00 Y500.00 Z100.00 E5000.00 ;Setup machine max acceleration
M203 X500.00 Y500.00 Z10.00 E50.00 ;Setup machine max feedrate
M204 P500.00 R1000.00 T500.00 ;Setup Print/Retract/Travel acceleration
M205 X8.00 Y8.00 Z0.40 E5.00 ;Setup Jerk
M220 S100 ;Reset Feedrate
M221 S100 ;Reset Flowrate
G92 E0 ;Reset Extruder
G1 E-5 F200; Retract filament to avoid bed levelling failure
G28 ;Home
G29 ; Bed leveling
M117 Purging Hotend
G1 X5 Y10 Z4 F5000.0 ;Move to start position
G1 Z0.30 F3000 ;Move Z Axis down
G1 X210 Y10 Z0.28 F1500.0 E15 ;Draw the first line
G1 X210 Y10.4 Z0.28 F5000.0 ;Move to side a little
G1 X5 Y10.4 Z0.28 F1500.0 E30 ;Draw the second line
G1 X5 Y10.8 Z0.28 F5000.0 ;Move to side a little
G1 X210 Y10.8 Z0.28 F1500.0 E45 ;Draw the third line
G92 E0 ;Reset Extruder
G1 Z4.0 F3000 ;Move Z Axis up
```

Gcode end : 
```
G91 ;Relative positionning
G1 E-4 F300 ;Retract a bit
G1 Z2 F2400 ;Raise Z
G1 E-35 F300 ;Retract a lot
G90 ;Absolute positionning

G1 X5 Y200 F5000 ;Present print
M106 S0 ;Turn-off fan
M104 S0 ;Turn-off hotend
M140 S0 ;Turn-off bed

M84 X Y E ;Disable all steppers but Z
M117 Finished !
```

# BL touch

During a print, slow double click, the lcd will prompt a menu. Rotate to the right to make the gap bigger, to the left for a smaller gap.

```
M851 ; note the number
M851 Z0 ; set the offset to zero
G28
G1 Z0
```

The LCD display should show Z = 0
From the display go to the Menu then Prepare/Move axis/0.1mm/Move Z
Now move the Z axis slowly down until the nozzle is the right distance from the build plate (folded piece of paper or thin card).
Note the Z axis value on the display it should be something like -1.5

```
M851 Z-1.5 ; to set the offset you got in the previous step.
M500 ; Stores the values in EEPROM so that it is not reset when you power the printer off and on.
```

If you find that you need to increase or decrease the gap then do:

```
M851 Z-1.4 ; this would make the gap bigger or
M851 Z-1.6 ; this would make the gap smaller
M500 ; to save the value to EEPROM
```
